import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-botones',
  templateUrl: './botones.component.html',
  styleUrls: ['./botones.component.css']
})
export class BotonesComponent implements OnInit {


  form!:FormGroup
  constructor(private fb:FormBuilder) { 
    this.cargarForm();
    console.log(this.form.controls);
    
  }

  ngOnInit(): void {
  }

  get utilesEscolares(){
    return this.form.get('utiles') as FormArray;
  }
  array:[][]=[[]];
  cargarForm(){
    this.form = this.fb.group({

      nombre:['',Validators.pattern(/^[a-zA-zñÑ\s]+$/)],
      check:[true],
      utiles: this.fb.array(this.array)
    })
  }

  adicionMaterial():void{
    this.utilesEscolares.push(this.fb.control(''));
  }

}